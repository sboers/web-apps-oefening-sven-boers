//AUTHENTICATION FACTORY
app.factory('auth', ['$http', '$window', function($http, $window) {
  var auth = {};
  
  //save token to localStorage
  auth.saveToken = function(token) {
    $window.localStorage['flapper-news-token'] = token;
  };
  
  //get token from localStorage
  auth.getToken = function() {
    return $window.localStorage['flapper-news-token'];
  };
  
  //check if user is logged in
  auth.isLoggedIn = function() {
    var token = auth.getToken();
    
    if(token) { //token exists => check if token has expired
      var payload = JSON.parse($window.atob(token.split('.')[1])); //payload = middle part of token between two '.' 's
      
      return payload.exp > Date.now() / 1000;
    } else {
      return false; //user is logged out
    }
  };
  
  //get username of logged in user
  auth.currentUser = function() {
    if(auth.isLoggedIn()) {
      var token = auth.getToken();
      var payload = JSON.parse($window.atob(token.split('.')[1]));
      
      return payload.username;
    }
  };
  
  //register new user
  auth.register = function(user) {
    return $http.post('/register', user).success(function(data) {
      auth.saveToken(data.token);
    });
  };
  
  //login user
  auth.logIn = function(user) {
    return $http.post('/login', user).success(function(data) {
      auth.saveToken(data.token);
    });
  };
  
  //logout user
  auth.logOut = function() {
    $window.localStorage.removeItem('flapper-news-token');
  };
  
  return auth;
}]);