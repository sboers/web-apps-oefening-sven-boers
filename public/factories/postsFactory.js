//POST FACTORY
app.factory('posts', ['$http', 'auth', function($http, auth) {
    var postFactory = {
      posts: []
    };

    //loading all posts 
		postFactory.getAll = function(){
			return $http.get('/posts').success(function(data) {
				angular.copy(data, postFactory.posts); //copy => create deep copy of returned data => updates $scope.posts in MainCtrl
			});
		};
    
    //creating new posts
    postFactory.create = function(post) {
      return $http.post('/posts', post, {
        headers: {Authorization: 'Bearer ' + auth.getToken() } //send JWT with Authorization header using the Bearer schema (specific schema for authorization headers)
      }).success(function(data){
        postFactory.posts.push(data); //add new post
      });
    };
    
    //upvoting posts
    postFactory.upvote = function(post) {
      return $http.put('/posts/' + post._id + '/upvote', null, {
        headers: {Authorization: 'Bearer ' + auth.getToken() }
      })
        .success(function(data){
          post.upvotes += 1;
        });
    };
    
    //get post by id
    postFactory.get = function(id) {
      return $http.get('/posts/' + id).then(function(res){
        return res.data;
      });
    }; 
    
    //add comment to post
    postFactory.addComment = function(id, comment) {
      return $http.post('/posts/' + id + '/comments', comment, {
        headers: {Authorization: 'Bearer ' + auth.getToken() }
      });
    };
    
    //upvoting comments
    postFactory.upvoteComment = function(post, comment) {
      return $http.put('/posts/' + post._id + '/comments/' + comment._id + '/upvote', null, {
        headers: {Authorization: 'Bearer ' + auth.getToken() }
      })
        .success(function(data) {
          comment.upvotes += 1;
        });
    };

    return postFactory;
  }
]);