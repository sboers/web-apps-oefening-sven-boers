app.controller('AuthCtrl', [
  '$scope',
  '$state',
  'auth',
  function($scope, $state, auth) {
    $scope.user = {}; //initialize user on $scope for form
    
    $scope.register = function() {    //register user
      auth.register($scope.user).error(function(error) {
        $scope.error = error;  
      }).then(function() {
        $state.go('home');
      });
    };
    
    $scope.logIn = function() {     //login user
      auth.logIn($scope.user).error(function(error){
        $scope.error = error;
      }).then(function(){
        $state.go('home');
      });
    };
  }
]);