app.controller('PostsCtrl', [
  '$scope',
  'posts',
  'post',
  'auth',
  function ($scope, posts, post, auth) {

    $scope.post = post;

    $scope.isLoggedIn = auth.isLoggedIn;

    $scope.addComment = function () {
      if (!$scope.body || $scope.body === '') {
        return;
      }

      posts.addComment(post._id, { //call addComment of posts service
        body: $scope.body,
        author: 'user',
      }).success(function (comment) {
        $scope.post.comments.push(comment); //add to comments
      });
      
      // $scope.post.comments.push({
      // 	author: 'user',
      // 	body: $scope.body,
      // 	upvotes: 0
      // });
      
      $scope.body = '';
    }
    
    //upvoting comments
    $scope.incrementUpvotes = function (comment) {
      posts.upvoteComment(post, comment);
    };


    $scope.imagePath = 'images/like_60_60.jpg';

  }
]);