app.controller('MainCtrl', [
  '$scope',
  'posts',
  'auth',
  '$mdDialog',
  function ($scope, postFactory, auth, $mdDialog) {
    
    var posts = postFactory;
    
    $scope.posts = postFactory.posts;

    $scope.isLoggedIn = auth.isLoggedIn;

    /*
    var addPost = function () {
      if (!$scope.title || $scope.title === '') {
        return;
      }

      postFactory.create({
        title: $scope.title,
        link: $scope.link,
        author: 'user'
      });

      $scope.title = '';
      $scope.link = '';
    };
    */

    $scope.incrementUpvotes = function (post) {
      //post.upvotes += 1;
      postFactory.upvote(post);
    };

    $scope.showDialog = function (ev) { //show add post dialog

      $mdDialog.show({
        controller: DialogController,
        templateUrl: 'templates/addPostDialog.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
      });

      function DialogController($scope, $mdDialog) {
        $scope.cancel = function () {
          $mdDialog.cancel();
        };

        $scope.addPost = function () {

          if (!$scope.title || $scope.title === '') {
            return;
          }

          posts.create({
            title: $scope.title,
            link: $scope.link,
            author: 'user'
          });

          $scope.title = '';
          $scope.link = '';


          $mdDialog.hide();
        };
      }

    };

    $scope.imagePath = 'images/like_60_60.jpg';

  }
]);

