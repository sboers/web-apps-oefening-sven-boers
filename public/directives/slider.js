/* Slide Show Plugin - Custom directive */
app.directive('slider', function ($timeout) {
  return {
    restrict: 'AE',
    replace: true,
    scope: {
      images: '='
    },
    link: function (scope, elem, attrs) {
      scope.currentIndex = 0; //initially the index is at the first image
      
      scope.next = function () {
        scope.currentIndex < scope.images.length - 1 ? scope.currentIndex++ : scope.currentIndex = 0;
      };

      scope.prev = function () {
        scope.currentIndex > 0 ? scope.currentIndex-- : scope.currentIndex = scope.images.length - 1;
      };

      scope.$watch('currentIndex', function () {
        scope.images.forEach(function (image) {
          image.visible = false; //make every image invisble
        });

        scope.images[scope.currentIndex].visible = true; //make current image visible
      });

      /* refresh each 5 seconds */
      var timer;
      var sliderFunc = function () {
        timer = $timeout(function () {
          scope.next();
          timer = $timeout(sliderFunc, 5000);
        }, 5000);
      };

      sliderFunc();

      scope.$on('$destroy', function () {
        $timeout.cancel(timer); // when the scope is getting destroyed, cancel the timer
      });

    },
    templateUrl: 'templates/directives/slider.tmpl.html'
  };

});