var app = angular.module('flapperNews', ['ui.router', 'ngMaterial', 'ngAnimate', 'ngMdIcons', 'ngMessages']);

app.config([
  '$stateProvider',
  '$urlRouterProvider',
  function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/home',
        templateUrl: '/templates/home.html',
        controller: 'MainCtrl',
				resolve: {
					postPromise: ['posts', function(postFactory){
						return postFactory.getAll();
					}],
				}
      })
      .state('posts', {
        url: '/posts/{id}',
        templateUrl: '/templates/posts.html',
        controller: 'PostsCtrl',
        resolve: {
          post: ['$stateParams', 'posts', function($stateParams, posts) {
            return posts.get($stateParams.id);
          }]
        }
      })
      .state('login', {
        url: '/login',
        templateUrl: '/templates/login.html',
        controller: 'AuthCtrl',
        onEnter: ['$state', 'auth', function($state, auth) { //onEnter => detect if user is authenticated before entering state
          if(auth.isLoggedIn()){
            $state.go('home');
          }
        }]
      })
      .state('register', {
        url: '/register',
        templateUrl: '/templates/register.html',
        controller: 'AuthCtrl',
        onEnter: ['$state', 'auth', function($state, auth) {
          if(auth.isLoggedIn()) {
            $state.go('home');
          }
        }]
      })
      .state('slider', {
        url: '/slider',
        templateUrl: '/templates/slider.html',
        controller: 'SliderCtrl'
      });
      
    $urlRouterProvider.otherwise('home');
  }
]);

app.config(function($mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('light-blue')
    .accentPalette('orange');
});