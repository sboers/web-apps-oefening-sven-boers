var express = require('express');
var router = express.Router();

//import mongoose + post & comment models
var mongoose = require('mongoose');
var Post = mongoose.model('Post');
var Comment = mongoose.model('Comment');

//require passport + import user model
var passport = require('passport');
var User = mongoose.model('User');

//require express-jwt + middelware for authenticating jwt tokens
var jwt = require('express-jwt');
var auth = jwt({ secret: 'SECRET', userProperty: 'payload' });


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

//GET all posts
router.get('/posts', function(req, res, next){ //req (=request): contains all info about request; res (=response): object to respond the client
  Post.find(function(err, posts){
    if(err) { 
      return next(err); //error handling
    }
    
    res.json(posts); //send retrieved posts back to client
    
  });
});

//CREATE a post
router.post('/posts', auth, function(req, res, next){  //requires authentication (auth)
  var post = new Post(req.body);
  
  post.author = req.payload.username;
  
  post.save(function(err, post){
    if(err) { 
      return next(err); //error handling
    }
    
    res.json(post); //send new post to client
    
  });
});

//FIND post by ID
router.param('post', function(req, res, next, id){
  var query = Post.findById(id);
  
  query.exec(function(err, post){
    if (err) {
      return next(err);
    }
    
    if(!post) {
      return next(new Error('can\'t find post'));
    }
    
    req.post = post; //attach retrieved post object from the database to request (req) object
    return next();
  });
});

//GET post by ID
router.get('/posts/:post', function(req, res, next){
  req.post.populate('comments', function(err, post){ //populate => load all comments associated with post
    if(err) {
      return next(err);
    }
    
    res.json(post);
  });
});

//upvote post
router.put('/posts/:post/upvote', auth, function(req, res, next) { //requires authentication (auth)
  req.post.upvote(function(err, post){
    if(err) {
      return next(err);
    }
    
    res.json(post); //send updated post to client
  });
});

//POST comment
router.post('/posts/:post/comments', auth, function(req, res, next) { //requires authentication (auth)
  var comment = new Comment(req.body);
  comment.post = req.post;
  
  comment.author = req.payload.username;
  
  comment.save(function(err, comment){
    if(err) {
      return next(err);
    }
    
    req.post.comments.push(comment); //add new comment to existing comments
    req.post.save(function(err, post){
      if(err) {
        return next(err);
      }
      
      res.json(comment); //send added comment to client
    });
  });
});

//upvote comment
router.put('/posts/:post/comments/:comment/upvote', auth, function(req, res, next) { //requires authentication (auth)
  req.comment.upvote(function(err, comment){
    if(err) {
      return next(err);
    }
    
    res.json(comment); //send updated comment to client
  });
});

//FIND comment by ID
router.param('comment', function(req, res, next, id){
  var query = Comment.findById(id);
  
  query.exec(function(err, comment){
    if (err) {
      return next(err);
    }
    
    if(!comment) {
      return next(new Error('can\'t find comment'));
    }
    
    req.comment = comment; //attach retrieved comment object from the database to request (req) object
    return next();
  });
});

//Register user
router.post('/register', function(req, res, next) {
  if(!req.body.username || !req.body.password){
    return res.status(400).json({ message: 'Please fill out all fields' });
  }
  
  var user = new User();
  
  user.username = req.body.username; //retrieve username
  
  user.setPassword(req.body.password); //retrieve password
  
  user.save(function(err) {
    if(err) {
      return res.status(400).json({ message: 'User already exists' });
    }
    
    return res.json({ token: user.generateJWT() }); //save new user
  });
});

//Login user
router.post('/login', function(req, res, next){
  if(!req.body.username || !req.body.password) {
    return res.status(400).json({ message: 'Please fill out all fields'});
  }
  
  passport.authenticate('local', function(err, user, info){
    if(err) {
      return next(err);
    }
    
    if(user) {
      return res.json({ token: user.generateJWT() });
    } else {
      return res.status(401).json(info);
    }
  })(req, res, next);
});

module.exports = router;