var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('User');

//authentication user with username & password
passport.use(new LocalStrategy(
	function(username, password, done) {
		User.findOne({ username: username }, function(err, user) {
			if(err) {
				return done(err);
			}
			
			if(!user) {
				return done(null, false, { message: 'Incorrect username.' });
			}
			
			if(!user.validPassword(password)) { //calls validPassword from user model
				return done(null, false, { message: 'Incorrect password.' });
			}
			
			return done(null, user);
		});
	}
));