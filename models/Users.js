var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
	username: { type: String, lowercase: true, unique: true },
	hash: String,
	salt: String
});

var crypto = require('crypto');
var jwt = require('jsonwebtoken');

UserSchema.methods.setPassword = function (password) {
	this.salt = crypto.randomBytes(16).toString('hex');

	this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex'); //set hash for password
};

UserSchema.methods.validPassword = function (password) {
	var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex'); //get hash for password

	return this.hash === hash;
};

UserSchema.methods.generateJWT = function() {
	//set expiration to 60 days
	var today = new Date();
	var exp = new Date(today);
	exp.setDate(today.getDate() + 60);
	
	return jwt.sign({ 							//first argrument => payload (id, username, expiration of token)
		_id: this._id,
		username: this.username,
		exp: parseInt(exp.getTime() / 1000), 
	}, 'SECRET');
};	

mongoose.model('User', UserSchema);