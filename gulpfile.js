var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');


var plugins = gulpLoadPlugins(); //see also: https://github.com/jackfranklin/gulp-load-plugins

var testFolder = './test';

gulp.task('runTests', function() {
	return gulp.src(testFolder + '/*.js')
		.pipe(plugins.mocha());
});