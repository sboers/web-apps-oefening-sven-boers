# README #

The following steps are necessary to run the application:

### Run the application ###

* start your MongoDb
* command prompt: **mongod**
* open the command prompt and navigate to the project directory
* command prompt: **npm start**


#### When the node modules aren't installed already: ####

* open the command prompt and navigate to the project directory
* command prompt: **npm install**